/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.bcuchos.conditional;

/**
 *
 * @author Bryan Alejandro Cucho Suyo <bryan.cucho.s@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Conditional Statemnets");

        int i = 35;
        if (30 < i) {
            System.out.println("El numero es mayor a 30");
        }
        if (i % 2 == 1) {
            System.out.println("Es impar");
        }
        if (i % 2 == 0) {
            System.out.println("Es par");
        }

           int j=29;
        if (30 < j) {
            System.out.println("El numero es mayor a 30");
        } else {

            if (j % 2 == 1) {
                System.out.println("Es impar");
            } else {
                if (j % 2 == 0) {
                    System.out.println("Es par");
                }
            }
        }
          
     /*
           System.out.println("eJERCISIO");
        int k=31;
        
        if (30 < k) {
            System.out.println("El numero es mayor a 30");
            return; //Sale de la funcion 
        }
        
        if (k % 2 == 1) {
            System.out.println("Es impar");
            return ;
        }
        if (k % 2 == 0) {
            System.out.println("Es par"); //SIn el else 
            return;
        }
        
        */
     
        int m=11;
        switch(i%2){
            case 0:
                System.out.println("Es par");
                break;
            case 1:
                System.out.println("Es impar");
                break;
            default:
                System.out.println("Cual quier otro numero ");
                break; //Que hace el switch 
        }   
    }
}
