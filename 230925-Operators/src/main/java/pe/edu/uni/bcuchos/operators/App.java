/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.bcuchos.operators;

import java.io.File;

/**
 *
 * @author Bryan Alejandro Cucho Suyo <bryan.cucho.s@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("-");
        System.out.println("Hello World!");
        System.out.println("+");
        
        
        int y1=4;
        double x1= 3+2 * --y1;
        System.out.println(x1);
        System.out.println(y1);
        y1--;
        //Aritmetic operators
        int x2= 2*5 + 3*4-8;
        System.out.println("x2=" +x2);
        
          int x3= 2* ((5 + 3)*4-8);
        System.out.println("x2=" +x3);
        System.out.println(10/3.0);
        System.out.println(10%3);
        
        int x4=1;
        long y4=33;
        System.out.println(x4*y4);
        System.out.println("bytes:" + Integer.SIZE/8);
        System.out.println("bytes:" + Long.SIZE/8);
        
        //La promocion la efectuan los operadores 
        double x5= 39.41;
        float y5= 13.5f;
        System.out.println(x5+y5);
        
        short x6=10;
        short y6=3; //Al trabajar con short , byte y char se promociona
        System.out.println(x6/y6); //Se pide solo el cociente
        
        short x7=17;
        float y7 =3;
        double z7=38;
        System.out.println(x7 *y7 / z7);
        
        boolean x8 =false;
        // clase ---->Booblean x9 = false;
        System.out.println("x8: " + x8);
        x8= !x8;
        System.out.println("x8: " +x8);
        
        double x9 = 1.21;
        System.out.println("x9_ "+ x9);
        //x9 = !x9; negacion pero no funciona pq no es booleano
        x9=-x9;
         System.out.println("x9_ "+ x9);
         
         int counter = 0;
         System.out.println("counter: " + counter);
         System.out.println("counter: " + ++counter);
         System.out.println("counter: " + counter);
         System.out.println("counter: " + counter--);
         System.out.println("counter: " + counter);
         
         int x=3;
         int y= ++x * 5 / x-- + --x;
         System.out.println("x: "  + x);
         System.out.println("y:  "  + y);
         
 //        int x=1.8;
   //      short y=1921112; 
   //       int z=9f;
       // long t=192301398093818323; //long 2^64
        
       int a1= (int)1.8; //Operador cast 
        System.out.println("a1: " + a1);
       short b1= (short) 1921222;
        System.out.println("b1"+ b1 ) ;
        int c1 = (int)9f;
        System.out.println("c1 " +c1);
        long d1 = 192301398193818323L;
        System.out.println("d1 "+ d1);
        
        short a2= 10;
        short b2= 3;
        short c2 = (short)(a2 *b2);
        System.out.println("c2 " +c2);
        
        int a3=2, b3=3;
        //a3=a3*b3; // asignacion simple
        a3 *=b2;  //Asignacion copmuesta 
        System.out.println("a3 " + a3);
        
        
        long a4=18;
        int b4=5; 
        b4 *= a4; //se castea la asignacion 
        System.out.println("b4" + b4); //Castea en la asignacion no antes ni despues 
        
        long a5=5;
        long b5= (a5=3) ;
        System.out.println("a5 "+a5);
        System.out.println("a5 "+b5);
        
        int a6=10,b6=20,c6= 30;
        System.out.println(a6 <b6); 
        System.out.println(a6 <= b6); 
        System.out.println(a6 >=b6); 
        System.out.println(a6 > b6);
        
        boolean a7 = false || (y<4); //Solo se evalua el primero si se puede para ahorrar pasos
        System.out.println("a7" +a7);
        
        Object o; 
        o = new Object();
        if(o!=null && o.hashCode() <5){
            //
        }
        if(o!=null & o.hashCode() <5){ //Se tiene que evaluar todo 
            //
        }
        int a8=6;
        boolean b8= (a8>=6) || (++a8<=7); //El resultado es 6 porque ya no evalua el segundo
        System.out.println("a8" + a8);
        
        boolean b9 = false;
        boolean a9 = (b9= true);
        System.out.println("a9" +a9);
        
        System.out.println("Equiality Operator ");
        //cuando es problema de compilacion subraya toda la linea, solo sale un foquito shiquit
        File p1 = new File("File.txt");
        File q1 = new File("File.txt");
        File r1=p1;
        System.out.println("pi == q1?" + (p1==p1));
        System.out.println("pi == q1?" + (r1==p1)); //SOn falsos porque se peude tener referencias de distintos lugares
    }
}
